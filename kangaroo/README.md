## (kanga)roo maps

### MountDoom2

A map ported from old classic game Jetmen Revival 1.2 with some adjustments made by myself. It's very big already so I reccomend to play it in normal (non-mirrored) mode (although u can also play it in extended mode). Maybe it's too big for duels or HTF, but it would be suitable in ffa. Created in gimp. Uploaded in 3 versions: normal (big), medium and small (even the small one is much bigger than standard 504x350 map size).

### asshat_temple

Kinda "easter egg" map. It refers to the name of the first webliero "clan" called [ASS] and its ancestor. Created in WormHole2 and then converted into png format using gimp.

### dali+ffa

My inspiration for creating this map was one of the paintings by Salvador Dali. Very bizarre but original. Created in gimp.

### giger3

My inspiration for creating this map was one of the paintings by H.R. Giger, the creator of Alien. The map is creepy, but fun to play. Created in WormHole2 and then converted into png format using gimp.

### gonad2

My original idea for a map. Created in WormHole2 and then converted into png format using gimp.

### got4

Another "easter egg" map which refers to famous webliero player SuckMyGauss and his ultimate enemy - noname turd. Created in WormHole2 and then converted into png format using gimp.

### guernica2

My original idea for a map, but I was inspired by famous Pablo Picasso's painting called Guernica (which was used as a background). Created in WormHole2 and then converted into png format using gimp.

### hexagonal

My original idea for a map. Created in WormHole2 and gimp.

### jetmen1+bunnyhop

Another map "borrowed" from old classic game Jetmen Revival 1.2 with some adjustments made by myself. Created in gimp.

### jetmen1 extended

Same map as "jetmen1+bunnyhop", but changed a bit to adjust it to extended mode. Created in gimp.

### lunar_dig

Another map ported from old classic game Jetmen Revival 1.2 with some adjustments made by myself. It's very big already so I reccomend to play it in normal (non-mirrored) mode (although u can also play it in extended mode). Maybe it's too big for duels or HTF, but it would be suitable in ffa. Created in gimp. Uploaded in 3 versions: normal (big), medium and small (even the small one is much bigger than standard 504x350 map size).

### vault_boy

My own idea of a map, however it was slightly inspired by 神風's map "despair". The map is kinda tricky, cuz it the floor is made of undefined material (so that bullets pass through), in order to prevent from spammy game. Created in WormHole2 and gimp.

### poo arena

A map ported from another Liero clone - gusanos, with some adjustments made by myself and changed colors. It is suitable for duels, but can be also played in ffa. Created in gimp.

### poo arena (extended)

Same map as "poo arena", but changed a bit to adjust it to extended mode. Created in gimp.

### blat2

Another map ported from gusanos, with some adjustments made by myself and changed colors. It is suitable for duels, but can be also played in ffa. Created in gimp.

### wtf2

Another map ported from gusanos, with some adjustments made by myself and changed colors. It is suitable for duels, but can be also played in ffa. Created in gimp.

### wtf2 (extended)

Same map as "wtf2", but changed a bit to adjust it to extended mode. Created in gimp.

### emo

A map ported from gusanos promode mod, with some adjustments made by myself and changed colors. Created in gimp.

### JDM

Another map ported from gusanos, with some adjustments made by myself and changed colors. Created in gimp.

### koala

Another map ported from gusanos, with some adjustments made by myself and changed colors. Created in gimp.

### val

Another map ported from gusanos, with some adjustments made by myself and changed colors. Created in gimp.

### dn_core

Another map ported from gusanos, with some adjustments made by myself and changed colors. Created in gimp.

### yennefer

My own idea for a map, but I used Yennefer's picture as a background. Created in WormHole2 and then converted into png format using gimp.
